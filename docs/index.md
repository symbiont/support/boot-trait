# Symbiont / Boot Trait

!!! warning
    This package is `work in progress`!

!!! Note
    This documentation only documents the technical usage of this package with little to no text.

Simple event dispatcher.

## Installation

Requires at least PHP `8.2`

```bash
composer require symbiont/support-boot-trait
```

---

## Testing

```php
composer test
```

## License

Released under MIT License