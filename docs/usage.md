# Usage

Compact version of Laravel's Model boot/initialize traits using `initialize{TraitName}()`.

## Guide lines

Each trait can implement a method `initialize{TraitName}`. These trait methods will be called when `bootTraits()` is 
issued.

 - Class implements the contract `Symbiont\Support\BootTrait\Contracts\BootsTraits`
 - Class uses the trait `Symbiont\Support\BootTrait\BootsTrait`
 - Any trait can implement a method starting with `initialize` followed by the traits name.

---

```php
<?php
use Symbiont\Support\BootTrait\Contracts\BootsTraits;
use Symbiont\Support\BootTrait\BootsTrait;

trait SomeTrait {

    protected mixed $property;

    protected function initializeSomeTrait() {
        $this->property = 'some-property-value';
    }

    public function getProperty() {
        return $this->property;
    }
    
    // trait methods...
}

class AwesomeClass implements BootsTraits {

    use BootsTrait,
        SomeTrait;

    public function __construct(string $something) {
        $this->bootTraits();
    }
 
}

$awesome = new Awesome;
$awesome->getProperty()
// will output `some-property-value`
```

### Constructor arguments

If needed, arguments can be passed per trait.

```php
<?php
use Symbiont\Support\BootTrait\Contracts\BootsTraits;
use Symbiont\Support\BootTrait\BootsTrait;
use Some\Package\Container;

trait SomeTrait {

    protected mixed $property;

    protected function initializeSomeTrait(mixed $property) {
        $this->property = $property;
    }

    // trait methods...
}

trait AnotherTrait {

    protected object $driver;

    protected function initializeAnotherTrait(object $driver) {
        $this->driver = $driver;
    }
    
}

class AwesomeClass implements BootsTraits {

    use BootsTrait, 
        SomeTrait,
        AnotherTrait;

    public function __construct(string $something, mixed $property) {
        $this->bootTraits([
            SomeTrait::class => [$property],
            AnotherTrait::class => [Container::get('some-driver')]
        ]);
    }
 
}
```

## Class inheritance and multiple `bootTraits()` call

If multiple `bootTraits()` are called in a chain of classes (inheritance), each `bootTraits()` can call specific traits
to influence the arguments being passed and avoid multiple calls to the same trait initializer.

The example below shows how to customize multiple classes calling the `bootTraits()` method.

```php
<?php
use Some\Driver;

trait FirstTrait {
    protected function initializeSecondTrait() {
        /* some logic .. */
    }
} // no initialize method
trait SecondTrait {
    protected string $property;
    // can be called without arguments
    protected function initializeSecondTrait(string $property = 'default') {
        $this->property = $property;
    }
}
trait ThirdTrait {
    protected Driver $driver;
    // cannot be called without adequate arguments
    protected function initializeThirdTrait(Driver $driver) {
        $this->driver = $driver;
    }
}

class FirstClass {

    use FirstTrait,
        SecondTrait;

    public function __construct() {
        $this->bootTraits();
        // by default calls all traits, also every trait
        // given from any child class of FirstClass
    }
     
}

class SecondClass extends FirstClass {

    use ThirdTrait;
    
    public function __construct() {
        // will call all traits
        $this->bootTraits([
            // ThirdTrait requires a driver object
            ThirdTrait::class => [ 'driver' => new Driver ]
        ]);
        
        parent::__construct();
        // parents bootTrait will have no traits left to call
    }
    
}

class ThirdClass extends SecondClass {

    public function __construct() {
        // will only call SecondTrait
        $this->bootTraits([
            SecondTrait::class => ['property' => 'overwrite-default']
        ], [
            // only call SecondTrait and leave the rest for the parents constructor call
            SecondTrait::class               
        ])
        
        parent::__construct();
        // parents bootTrait will have ThirdTrait left to call
    }
    
}

$third = new ThirdClass;
$second = new SecondClass;
$first = new FirstClass;
```

---

The call sequence for `$third = new ThirdClass` will be:

 - `ThirdClass` calls `bootTraits` specifying only to initialize `SecondTrait` with given arguments. `ThirdTrait` and `FirstTrait` remains to be called.
     - Calls `SecondClass` constructor.
 - `SecondClass` calls `bootTraits` only specifying a required argument for `ThirdTrait`, calling remaining `ThirdTrait` and `FirstTrait`.
     - Calls `FirstClass` constructor
 - `FirstClass` calls `bootTraits` with no traits left to call. 
     - `SecondTrait` has been called by `ThirdClass`,
     - `ThirdTrait` and `FirstTrait` have been called by `SecondClass`.

---

The call sequence for `$second = new SecondClass` will be:

- `SecondClass` calls `bootTraits` only specifying a required argument for `ThirdTrait`, calling `ThirdTrait`, `SecondTrait` and `FirstTrait`.
    - Calls `FirstClass` constructor
- `FirstClass` calls `bootTraits` with no traits left to call.
    - `ThirdTrait`, `SecondTrait` and `FirstTrait` have been called by `SecondClass`.

---

The call sequence for `$first = new FirstClass` will be:

- `FirstClass` calls `bootTraits` calling `SecondTrait` and `FirstTrait`.

---

### Traits calling `bootTraits()`

A trait can call `bootTraits()` just as any class can. The order of traits in the `use` clause of a class is important. 
The rule here is to `use` the trait calling `bootTraits()` before any other trait.


```php
<?php

use Symbiont\Support\BootTrait\Contracts\BootsTraits;
use Symbiont\Support\BootTrait\BootsTrait;

trait FirstTrait {
    protected string $property;

    protected function initializeFirstTrait(string $property = 'some-default') {
        $this->property = $property;
    }
}

trait CallingBootTrait {
    use FirstTrait;

    protected function initializeCallingBootTrait() {
        $this->bootTraits([
            FirstTrait::class => ['overwrite-default']
        ]);
    }
}

class FirstClass implements BootsTraits {
    use BootsTrait,
        CallingBootTrait, // called first
        FirstTrait; // skipped because it was handled in CallingBootTrait::initializeCallingBootTrait()

    public function __construct() {
        $this->bootTraits();
    }
}

new FirstClass;
```

The call sequence for `bootTraits()`:

 - `FirstClass` calls `bootTraits()` in its constructor
     - The first trait in line is `CallingBootTrait`
 - `CallingBootTrait` calls `bootTraits()` passing an argument to `FirstTrait`

--- 

If the sequence gets mixed up, and e.g. the order of traits would be

```php
<?php
class FirstClass implements BootsTraits {
    use BootsTrait,
        FirstTrait,
        CallingBootTrait;
}
```

Then `FirstClass` constructor calling `bootTraits` will call `FirstTrait` and will be skipped for any other
`bootTraits()` calls nonetheless.