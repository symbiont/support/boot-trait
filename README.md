# Symbiont / Boot Trait

Compact version of Laravel's boot/initialize traits using `initializeTraitName()`.

This package is work in progress!

## Requirements

- `php 8.2`

## Installation

```bash
composer require symbiont/support-boot-trait
```

## Usage

Simple example of using `BootsTrait`.

```php

use Symbiont\Support\BootTrait\Contracts\BootsTraits;
use Symbiont\Support\BootTrait\BootsTrait;

trait SomeTrait {

    protected string $property;

    protected function initializeSomeTrait(bool $option) {
        $this->property = if($option) ?
            'whoohoo!' : 'booya!';
    }

    public function getProperty() {
        
    }
}

class AwesomeClass implements BootsTraits {

    use SomeTrait,
        BootsTrait;

    public function __construct(string $something, bool $option) {
        $this->bootTraits([
            SomeTrait::class => [$option]
        ]);
    }
 
}

(new Awesome('test', true))->getProperty();
// will output 'whoohoo!'
```

## Documentation

This documentation only documents the technical usage of this package with little to no text.

- Documentation: [https://symbiont.gitlab.io/support/boot-trait](https://symbiont.gitlab.io/support/boot-trait)
- Gitlab: [https://gitlab.com/symbiont/support/boot-trait](https://gitlab.com/symbiont/support/boot-trait)

## Tests

```bash
composer test
```

---

## License

[MIT license](LICENSE.MD)