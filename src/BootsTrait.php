<?php

namespace Symbiont\Support\BootTrait;

/**
 * Short version of Laravel Models boot and initialize principle
 * @source: https://github.com/laravel/framework/blob/33424bef66b1b16dbf6323f6f3dd75e44d488bc2/src/Illuminate/Database/Eloquent/Model.php#L290
 * @source: https://github.com/laravel/framework/blob/33424bef66b1b16dbf6323f6f3dd75e44d488bc2/src/Illuminate/Database/Eloquent/Model.php#L322
 */
trait BootsTrait {

    protected static array $traits_initialized = [];
    protected static array $trait_initializers = [];
    protected array $trait_initialized = [];

    /**
     * @return void
     */
    protected function bootTraits(array $trait_args = [], array $traits = []) {
        $class = static::class;

        if (! isset(static::$traits_initialized[$class])) {
            static::$traits_initialized[$class] = true;

            foreach (class_uses_recursive($class) as $trait) {
                if($trait === 'BootsTrait') {
                    continue;
                }

                if(! array_key_exists($class, static::$trait_initializers)) {
                    static::$trait_initializers[$class] = [];
                }

                if(array_key_exists($trait, static::$trait_initializers[$class])) {
                    continue;
                }

                if(method_exists($class, $initializer = 'initialize' . basename(str_replace('\\', '/', $trait)))) {
                    static::$trait_initializers[$class][$trait] = $initializer;
                }

            }
        }

        $this->initializeTraits($trait_args, $traits);
    }

    /**
     * Initialize traits
     * @return void
     */
    protected function initializeTraits(array $trait_args = [], array $traits = []) {
        $class = static::class;

        foreach (static::$trait_initializers[$class] as $trait => $method) {
            if(isset($this->trait_initialized[$method])) {
                continue;
            }

            if($traits && ! in_array($trait, $traits)) {
                continue;
            }

            $this->trait_initialized[$method] = true;

            array_key_exists($trait, $trait_args) ?
                call_user_func_array([$this, $method], $trait_args[$trait]) :
                $this->{$method}();
        }

    }

}