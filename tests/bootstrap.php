<?php

function includeIfExists($file) {
    return file_exists($file) ?
        require_once $file : false;
}

if ((!$loader = includeIfExists(__DIR__.'/../vendor/autoload.php')) && (!$loader = includeIfExists(__DIR__.'/../../../autoload.php'))) {
    echo 'Missing project dependencies.';
    exit;
}