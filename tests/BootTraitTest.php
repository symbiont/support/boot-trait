<?php

namespace Symbiont\Support\BootTrait\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Support\BootTrait\Tests\Classes\StaticClass;
use Symbiont\Support\BootTrait\Tests\Classes\TestClass;

final class BootTraitTest extends TestCase {

    public function testInitalizeTraitCalled() {
        $some = new TestClass;

        $this->assertFalse(in_array('initializeBootsTrait', StaticClass::$called));
        $this->assertTrue(in_array('initializeSomeTrait', StaticClass::$called));
        $this->assertTrue(in_array('initializeAnotherTrait', StaticClass::$called));
        $this->assertTrue(count(array_filter(StaticClass::$called, function($initializer) { return $initializer === 'initializeAnotherTrait'; })) === 1);
    }

}