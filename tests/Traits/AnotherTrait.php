<?php

namespace Symbiont\Support\BootTrait\Tests\Traits;

use Symbiont\Support\BootTrait\Tests\Classes\StaticClass;

trait AnotherTrait {

    protected function initializeAnotherTrait() {
        StaticClass::$called[] = __FUNCTION__;
    }

    protected function initializeThisTrait() {
        StaticClass::$called[] = __FUNCTION__;
    }
}
