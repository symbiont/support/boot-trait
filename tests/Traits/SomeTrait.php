<?php

namespace Symbiont\Support\BootTrait\Tests\Traits;

use Symbiont\Support\BootTrait\Tests\Classes\StaticClass;

trait SomeTrait {

    protected function initializeSomeTrait() {
        StaticClass::$called[] = __FUNCTION__;
    }

}
