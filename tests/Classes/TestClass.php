<?php

namespace Symbiont\Support\BootTrait\Tests\Classes;

use Symbiont\Support\BootTrait\BootsTrait;
use Symbiont\Support\BootTrait\Contracts\BootsTraits;
use Symbiont\Support\BootTrait\Tests\Traits\AnotherTrait;
use Symbiont\Support\BootTrait\Tests\Traits\SomeTrait;

class TestClass implements BootsTraits {

    use BootsTrait,
        SomeTrait,
        AnotherTrait;

    public function __construct() {
        $this->bootTraits();
    }
}